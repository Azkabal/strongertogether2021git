using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NimphaAttack : MonoBehaviour
{
    [SerializeField] private BodyMover bodyMover= null;
    [SerializeField] private EnergyLevel energyLevel = null;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!bodyMover.IsMoving()) return;

        if (!collision.transform.TryGetComponent(out EnergySource energySource)) return;

        GetEnergy(energySource);
    }

    private void GetEnergy(EnergySource energySource)
    {
        energyLevel.addEnergy(energySource.GetEnergy());

        Destroy(energySource.gameObject);
    }
}
