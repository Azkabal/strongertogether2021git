using UnityEngine;

public class BodyMover : MonoBehaviour
{
    [SerializeField] private Transform parentTransform = null;
    [SerializeField] private Rigidbody2D myRigidbody = null;

    //TODO [SerializeField] private enum BODYTYPE

    [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private float timeToMove = 0.5f;

    private bool isMoving = false;

    public bool IsMoving()
    {
        return isMoving;
    }


    public void TryMove(Vector3 positionToMove)
    {
        if (isMoving) return;
        //stop current velocity
        myRigidbody.velocity = Vector2.zero;

        Vector2 positionToMove2D = new Vector2(positionToMove.x, positionToMove.y);
        Vector2 myPosition2D = new Vector2(parentTransform.position.x, parentTransform.position.y);
        Vector2 forceVector = positionToMove2D - myPosition2D;

        forceVector.Normalize();

        //apply movement impulse
        myRigidbody.AddForce(forceVector * movementSpeed, ForceMode2D.Impulse);

        isMoving = true;
        Invoke("StopMovement", timeToMove);
    }

    private void StopMovement()
    {
        //stop current velocity
        myRigidbody.velocity = Vector2.zero;

        isMoving = false;
    }

    internal void TryLookAtMouse(Vector3 pz)
    {
        Vector2 direction = new Vector2(
            pz.x - parentTransform.position.x,
            pz.y - parentTransform.position.y);

        parentTransform.up = direction;
    }
}
