using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class EnergySource : MonoBehaviour
{
    [Range(1,100)]
    [SerializeField] private int maxEnergy = 2;
    private int energyAmount = 0;
    [SerializeField] private SpriteRenderer spriteRenderer = null;
    [SerializeField] private Light2D light = new Light2D();

    // Start is called before the first frame update
    void Start()
    {
        energyAmount = (int)Random.Range((int)1, (int)(maxEnergy+1));
        GetComponent<Rigidbody2D>().AddForce(
            new Vector2(Random.Range(0,1 * Random.Range(5,15)), Random.Range(0, 1 * Random.Range(5, 15))),
            ForceMode2D.Impulse);
        transform.Rotate(new Vector3(0, 0, 1), Random.Range(-360, 360) * Time.deltaTime);

        Color setColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        spriteRenderer.color = setColor;
        light.color = setColor;
    }

    internal int GetEnergy()
    {
        return energyAmount;
    }
}
