using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergySourceGenerator : MonoBehaviour
{
    [SerializeField] private GameObject prefab = null;
    [SerializeField] private Transform layerTransform = null;
    [SerializeField] private float x_size = 0f;
    [SerializeField] private float y_size = 0f;
    [SerializeField] private int generateAtStart = 50;
    [SerializeField] private float generateInterval = 3f;
    [SerializeField] private int generatePerInterval = 1;

    [SerializeField] private int maxParticles = 100;

    private float lastTime = 0f;

    private void Start()
    {
        if(prefab == null)
        {
            Debug.LogError("prefab == null"); return;
        }
        if (layerTransform == null)
        {
            Debug.LogError("layerTransform == null"); return;
        }

        for (int ix = 0; ix < generateAtStart; ix++)
        {
            GenerateEnergySource();
        }
    }

    private void Update()
    {
        if(Time.time > lastTime + generateInterval)
        {
            lastTime = Time.time;

            EnergySource[] sources = FindObjectsOfType<EnergySource>();
            if (sources.Length >= maxParticles) return;

            for(int ix = 0; ix < generatePerInterval; ix++)
                {
                    GenerateEnergySource();
                }
            }
    }

    private void GenerateEnergySource()
    {
        Vector3 position = new Vector3(Random.Range(-x_size / 2, x_size / 2), Random.Range(-y_size / 2, y_size / 2), 1);
        GameObject source = Instantiate(prefab, position, Quaternion.identity);
        source.transform.parent = layerTransform;
        source.transform.position = new Vector3(position.x, position.y, source.transform.parent.position.z - 1);
    }
}
