using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyLevel : MonoBehaviour
{
    [SerializeField] private int[] energyLevelRequirments = null;

    private int currentEnergy = 0;
    private int currentEnergyLevel = 0;

    public void addEnergy(int amount)
    {
        currentEnergy += amount;

        if (currentEnergyLevel == (energyLevelRequirments.Length - 1)) return;

        if(currentEnergy > energyLevelRequirments[currentEnergyLevel])
        {
            currentEnergyLevel += 1;

            //TODO: call upgrade function
        }
    }
}
