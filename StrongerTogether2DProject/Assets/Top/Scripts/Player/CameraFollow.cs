using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Func<Vector3> GetCameraPositionFunc;

    public void Setup(Func<Vector3> GetCameraPositionFunc)
    {
        this.GetCameraPositionFunc = GetCameraPositionFunc;
    }

    private void Update()
    {
        Vector3 cameraFollowPosition = GetCameraPositionFunc();
        cameraFollowPosition.z = transform.position.z;

        Vector3 cameraMoveDirection = (cameraFollowPosition - transform.position).normalized;
        float distance = Vector3.Distance(cameraFollowPosition, transform.position);
        float cameraSpeed = 1f;

        if(distance > 0)
        {
            Vector3 newCameraPosition = transform.position + cameraMoveDirection * distance * cameraSpeed * Time.deltaTime;

            float distanceAfterMoving = Vector3.Distance(newCameraPosition, cameraFollowPosition);

            if(distanceAfterMoving > distance)
            {
                //camera overhoot player
                newCameraPosition = cameraFollowPosition;
            }

            transform.position = newCameraPosition;
        }
    }
}
