
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] BodyMover bodyMover;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown((int)1))
        {
            Debug.Log("Mouse");
            ProccessRightMouseClick();
            return;
        }


        ProccessLookAtMouse();
    }

    private void ProccessRightMouseClick()
    {
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = transform.position.z;

        bodyMover.TryMove(pz);
    }

    private void ProccessLookAtMouse()
    {
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = transform.position.z;

        bodyMover.TryLookAtMouse(pz);
    }
}
